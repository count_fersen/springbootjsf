CREATE TABLE public.customer
(
  id text NOT NULL DEFAULT generate_random_string(),
  first_name text,
  last_name text,
  address_id text,
  date_of_birth date,
  CONSTRAINT customer_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.customer
  OWNER TO postgres;
