CREATE TABLE public.city
(
  id text NOT NULL DEFAULT generate_random_string(),
  name text,
  CONSTRAINT "PK_CITY" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.city
  OWNER TO postgres;

"1";"Surabaya"
"2";"Surakarta"
"3";"Semarang"
"4";"Situbondo"
"5";"Ngawi"
"6";"Nganjuk"
"7";"Medan"
"8";"Manado"
"9";"Makassar"
"10";"Manokwari"