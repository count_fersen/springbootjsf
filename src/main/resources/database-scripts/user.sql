CREATE TABLE public."users"
(
  id text NOT NULL DEFAULT generate_random_string(),
  username text,
  password text,
  roles text,
  CONSTRAINT "PK_USERS" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."users"
  OWNER TO postgres;

"01iOjsRZTXmgi8NaFfQd";"user" ;"ICy5YqxZB1uWSwcVLSNLcA==";"USER"
"18qlyZt82YP2obcrVOav";"admin";"ICy5YqxZB1uWSwcVLSNLcA==";"ADMIN"
"k1rE7oUA5TEnwceFEy6U";"super";"ICy5YqxZB1uWSwcVLSNLcA==";"ADMIN,USER"

password "ICy5YqxZB1uWSwcVLSNLcA==" adalah 123