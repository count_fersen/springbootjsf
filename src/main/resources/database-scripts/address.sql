CREATE TABLE public.address
(
  id text NOT NULL,
  address text,
  city text,
  postal_code text,
  CONSTRAINT "ADDRESS_PK" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.address
  OWNER TO postgres;
