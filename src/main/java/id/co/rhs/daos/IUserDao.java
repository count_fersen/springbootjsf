package id.co.rhs.daos;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import id.co.rhs.models.User;

/**
 * A DAO class for User data model.
 *
 * @author Rochmat Santoso
 * */
@Mapper
public interface IUserDao {

    /**
     * Get user by username.
     *
     * @param username the username
     * @return the user object
     * */
    User getUserByUsername(@Param("username") String username);

}
