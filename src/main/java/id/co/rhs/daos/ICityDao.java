package id.co.rhs.daos;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import id.co.rhs.models.City;

/**
 * A DAO class for City data model.
 *
 * @author Rochmat Santoso
 * */
@Mapper
public interface ICityDao {

    /**
     * Get city filtered by name.
     *
     * @param name city name
     * @param limit total row to select
     * @return all filtered city
     * */
    List<City> getCityByNameLike(
            @Param("name") String name, @Param("limit") int limit);

}
