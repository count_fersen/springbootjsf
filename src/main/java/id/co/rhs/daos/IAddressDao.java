package id.co.rhs.daos;

import org.apache.ibatis.annotations.Mapper;

import id.co.rhs.models.Address;

/**
 * A DAO class for Address data model.
 *
 * @author Rochmat Santoso
 * */
@Mapper
public interface IAddressDao {

    /**
     * Insert address.
     *
     * @param address address object to insert
     * */
    void insertAddress(Address address);

    /**
     * Update address.
     *
     * @param address address object to update
     * */
    void updateAddress(Address address);

    /**
     * Delete address.
     *
     * @param address address object to delete
     * */
    void deleteAddress(Address address);

}
