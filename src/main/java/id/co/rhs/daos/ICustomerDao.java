package id.co.rhs.daos;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import id.co.rhs.models.Customer;

/**
 * A DAO class for Customer data model.
 *
 * @author Rochmat Santoso
 * */
@Mapper
public interface ICustomerDao {

    /**
     * Get all customers.
     *
     * @param offset start row to select
     * @param limit total row to select
     * @return all customers
     * */
    List<Customer> getAllCustomers(
            @Param("offset") int offset, @Param("limit") int limit);

    /**
     * Get customer by primary key.
     *
     * @param id primary key
     * @return the customer
     * */
    Customer getCustomerById(@Param("id") String id);

    /**
     * Get count of all customers.
     *
     * @return total number of customer
     * */
    int getAllCustomersCount();

    /**
     * Insert customer.
     *
     * @param customer customer object to insert
     * */
    void insertCustomer(Customer customer);

    /**
     * Update customer.
     *
     * @param customer customer object to update
     * */
    void updateCustomer(Customer customer);

    /**
     * Delete customer.
     *
     * @param customer customer object to delete
     * */
    void deleteCustomer(Customer customer);

}
