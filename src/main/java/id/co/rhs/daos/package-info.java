/**
 * A package that contains all data access object classes.
 *
 * @author Rochmat Santoso
 */
package id.co.rhs.daos;
