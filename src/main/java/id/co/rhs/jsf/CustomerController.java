package id.co.rhs.jsf;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import id.co.rhs.daos.IAddressDao;
import id.co.rhs.daos.ICityDao;
import id.co.rhs.daos.ICustomerDao;
import id.co.rhs.jsf.datatable.CustomerDataModel;
import id.co.rhs.models.Address;
import id.co.rhs.models.City;
import id.co.rhs.models.Customer;
import id.co.rhs.spring.messages.KafkaSender;
import id.co.rhs.spring.messages.MessageSender;
import id.co.rhs.utils.CommonUtil;

/**
 * A controller class for all customer related operation.
 *
 * @author Rochmat Santoso
 * */
@Named("customerController")
@ViewScoped
public class CustomerController extends BaseController {

    /**
     * Customer DAO bean.
     * */
    @Autowired
    private ICustomerDao customerDao;

    /**
     * Address DAO bean.
     * */
    @Autowired
    private IAddressDao addressDao;

    /**
     * City DAO bean.
     * */
    @Autowired
    private ICityDao cityDao;

    /**
     * JMS bean.
     * */
    @Autowired
    private MessageSender messageSender;

    @Autowired
    private KafkaSender kafkaSender;

    /**
     * Customer data source.
     * */
    private CustomerDataModel customerDataModel;

    /**
     * Customer object.
     * */
    private Customer customer;

    /**
     * Selected customer on data table.
     * */
    private Customer selectedCustomer;

    /**
     * Max date that user can select.
     * */
    private String dateOfBirthMax;

    /**
     * Date format.
     * */
    private String dateFormat;

    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @param paramCustomer the customer to set
     */
    public void setCustomer(final Customer paramCustomer) {
        this.customer = paramCustomer;
    }

    /**
     * @return the customerDataModel
     */
    public CustomerDataModel getCustomerDataModel() {
        return customerDataModel;
    }

    /**
     * @param paramCustomerDataModel the customerDataModel to set
     */
    public void setCustomerDataModel(
            final CustomerDataModel paramCustomerDataModel) {
        this.customerDataModel = paramCustomerDataModel;
    }

    /**
     * @return the selectedCustomer
     */
    public Customer getSelectedCustomer() {
        return selectedCustomer;
    }

    /**
     * @param paramSelectedCustomer the selectedCustomer to set
     */
    public void setSelectedCustomer(final Customer paramSelectedCustomer) {
        this.selectedCustomer = paramSelectedCustomer;
    }

    /**
     * @return the dateOfBirthMax
     */
    public String getDateOfBirthMax() {
        return dateOfBirthMax;
    }

    /**
     * @param paramDateOfBirthMax the dateOfBirthMax to set
     */
    public void setDateOfBirthMax(final String paramDateOfBirthMax) {
        this.dateOfBirthMax = paramDateOfBirthMax;
    }

    /**
     * @return the dateFormat
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * @param paramDateFormat the dateFormat to set
     */
    public void setDateFormat(final String paramDateFormat) {
        this.dateFormat = paramDateFormat;
    }

    /**
     * Initialize object for first use.
     * */
    @PostConstruct
    public void postConstruct() {
        if (customer == null) {
            customer = new Customer();
            customer.setAddress(new Address());
        }
        if (customerDataModel == null) {
            customerDataModel = new CustomerDataModel();
            customerDataModel.setCustomerDao(customerDao);
        }
        if (dateOfBirthMax == null) {
            dateOfBirthMax = CommonUtil.formatDate(new Date());
        }
        if (dateFormat == null) {
            dateFormat = CommonUtil.DATE_FORMAT.toPattern();
        }
    }

    /**
     * Get filtered city.
     *
     * @param query the query to pass to filter
     * @return a list of filtered city
     * */
    public List<String> getFilteredCity(final String query) {
        List<String> result = new ArrayList<>();
        List<City> cities = cityDao.getCityByNameLike("%" + query + "%", 10);
        for (City city : cities) {
            result.add(city.getName());
        }
        return result;
    }

    /**
     * Save customer object.
     * */
    @Transactional
    public void addCustomer() {
        if (customer != null
                && (customer.getId() == null || customer.getId().isEmpty())) {
            Address addressObj = customer.getAddress();
            if (addressObj != null && addressObj.getAddress() != null && !addressObj.getAddress().isEmpty()) {
                // save address
                addressDao.insertAddress(addressObj);
            }
            // save customer
            customerDao.insertCustomer(customer);

            // send message to mq
            messageSender.send(customer);

            // send message to kafka
            kafkaSender.send(customer);

            // display success message
            addSuccessMessage("Customer data has been saved");
        } else {
            addErrorMessage("Failed saving customer data");
        }
    }

    /**
     * Update customer.
     * */
    @Transactional
    public void updateCustomer() {
        if (customer != null && customer.getId() != null && !customer.getId().isEmpty()) {
            Address addressObj = customer.getAddress();
            if (addressObj != null && addressObj.getId() != null) {
                // update address
                addressDao.updateAddress(customer.getAddress());
            } else if (addressObj != null && addressObj.getAddress() != null && !addressObj.getAddress().isEmpty()) {
                // insert address
                addressDao.insertAddress(addressObj);
            }

            // update customer
            customerDao.updateCustomer(customer);

            // display success message
            addSuccessMessage("Customer data has been updated");
        } else {
            addErrorMessage("Failed updating customer data");
        }
    }

    /**
     * Delete customer.
     * */
    @Transactional
    public void deleteCustomer() {
        if (customer != null && customer.getId() != null && !customer.getId().isEmpty()) {
            addressDao.deleteAddress(customer.getAddress());
            customerDao.deleteCustomer(customer);

            // display success message
            addSuccessMessage("Customer data has been deleted");
        } else {
            addErrorMessage("Failed deleting customer data");
        }
    }

    /**
     * A method that is called on data table selection.
     *
     * @param event a select event
     * */
    public void onRowSelect(final SelectEvent event) {
        customer = customerDao.getCustomerById(selectedCustomer.getId());
        if (customer.getAddress() == null) {
            customer.setAddress(new Address());
        }
        addSuccessMessage(customer.toString());
    }

}
