package id.co.rhs.jsf;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import id.co.rhs.models.User;
import id.co.rhs.utils.CommonUtil;

/**
 * A controller class for all operations on menu page.
 *
 * @author Rochmat Santoso
 * */
@Named("menuController")
@SessionScoped
public class MenuController {

    /**
     * User object.
     * */
    private User user;

    /**
     * Selected theme.
     * */
    private String selectedTheme;

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param paramUser the user to set
     */
    public void setUser(final User paramUser) {
        this.user = paramUser;
    }

    /**
     * @return the selectedTheme
     */
    public String getSelectedTheme() {
        return selectedTheme;
    }

    /**
     * @param paramSelectedTheme the selectedTheme to set
     */
    public void setSelectedTheme(final String paramSelectedTheme) {
        this.selectedTheme = paramSelectedTheme;
    }

    /**
     * Initialize object for first use.
     * */
    @PostConstruct
    public void postConstruct() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (user == null && authentication != null) {
            user = new User();
            user.setUsername(authentication.getName());
        }
        if (selectedTheme == null) {
            selectedTheme = CommonUtil.DEFAULT_THEME;
        }
    }

    /**
     * Change theme.
     *
     * @param e an event that occurs when changing theme
     * */
    public void changeTheme(final ValueChangeEvent e) {
        // do nothing here
    }

}
