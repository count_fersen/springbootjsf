package id.co.rhs.jsf.datatable;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;

import id.co.rhs.daos.ICustomerDao;
import id.co.rhs.models.Customer;

/**
 * A lazy data model class for customer.
 *
 * @author Rochmat Santoso
 * */
public class CustomerDataModel extends LazyDataModel<Customer> {

    /**
     *
     */
    private static final long serialVersionUID = -8167760059619295982L;

    /**
     * Customer DAO bean.
     * */
    @Autowired
    private ICustomerDao customerDao;

    /**
     * @param paramCustomerDao the customerDao to set
     */
    public void setCustomerDao(final ICustomerDao paramCustomerDao) {
        this.customerDao = paramCustomerDao;
    }

    @Override
    public final List<Customer> load(
            final int first,
            final int pageSize,
            final String sortField,
            final SortOrder sortOrder,
            final Map<String, Object> filters) {
        // set total row
        int customerCount = customerDao.getAllCustomersCount();
        setRowCount(customerCount);

        // load data
        return customerDao.getAllCustomers(first, pageSize);
    }

    @Override
    public final Object getRowKey(final Customer object) {
        return object.getId();
    }

    @Override
    public final Customer getRowData(final String rowKey) {
        return customerDao.getCustomerById(rowKey);
    }

}
