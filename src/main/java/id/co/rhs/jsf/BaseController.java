package id.co.rhs.jsf;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import id.co.rhs.utils.CommonUtil;

/**
 * Base class for all controller classes.
 *
 * @author Rochmat Santoso
 * */
public class BaseController {

    /**
     * Add success message to display.
     *
     * @param message the message to display
     * */
    protected void addSuccessMessage(final String message) {
        if (message != null && FacesContext.getCurrentInstance() != null) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_INFO,
                    message,
                    null);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Add error message to display.
     *
     * @param message the message to display
     * */
    protected void addErrorMessage(final String message) {
        if (message != null && FacesContext.getCurrentInstance() != null) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    message,
                    null);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Format a date.
     *
     * @param date the date to format
     * @return formatted date
     * */
    public String formatDate(final Date date) {
        if (date == null) {
            return null;
        }
        return CommonUtil.formatDate(date);
    }

}
