package id.co.rhs.spring;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.co.rhs.utils.CommonUtil;

/**
 * A class that holds configuration in application.properties file.
 *
 * @author Rochmat Santoso
 * */
@Configuration
@EnableAutoConfiguration
public class AppConfiguration {

    /**
     * Database driver class name.
     * */
    @Value("${spring.datasource.driver-class-name}")
    private String databaseDriverClassName;

    /**
     * Database connection url.
     * */
    @Value("${spring.datasource.url}")
    private String datasourceUrl;

    /**
     * Database connection username.
     * */
    @Value("${spring.datasource.username}")
    private String databaseUsername;

    /**
     * Database connection password.
     * */
    @Value("${spring.datasource.password}")
    private String databasePassword;

    /**
     * Build a datasource based on database configuration.
     *
     * @return a datasource bean for spring
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    @Bean
    public DataSource datasource()
            throws InvalidKeyException,
                   UnsupportedEncodingException,
                   NoSuchAlgorithmException,
                   NoSuchPaddingException,
                   IllegalBlockSizeException,
                   BadPaddingException {
        return DataSourceBuilder.create()
                .username(databaseUsername)
                .password(getSecurePassword())
                .url(datasourceUrl)
                .driverClassName(databaseDriverClassName).build();
    }

    /**
     * Get database password.
     *
     * @return the database password
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    private String getSecurePassword()
            throws InvalidKeyException,
                   UnsupportedEncodingException,
                   NoSuchAlgorithmException,
                   NoSuchPaddingException,
                   IllegalBlockSizeException,
                   BadPaddingException {
        return CommonUtil.decrypt(databasePassword);
    }

}
