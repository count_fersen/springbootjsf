package id.co.rhs.spring.security;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.password.PasswordEncoder;

import id.co.rhs.utils.CommonUtil;

/**
 * An implementation of {@link PasswordEncoder} to encode password.
 *
 * @author Rochmat Santoso
 * */
public class PasswordEncoderImpl implements PasswordEncoder {

    @Override
    public final String encode(final CharSequence rawPassword) {
        try {
            return CommonUtil.encryptMD5(rawPassword.toString());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public final boolean matches(
            final CharSequence rawPassword, final String encodedPassword) {
        try {
            String encodedPwd = CommonUtil.encryptMD5(rawPassword.toString());
            if (encodedPwd.equals(encodedPassword)) {
                return true;
            }
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

}
