/**
 * A package that contains all spring security classes.
 *
 * @author Rochmat Santoso
 */
package id.co.rhs.spring.security;
