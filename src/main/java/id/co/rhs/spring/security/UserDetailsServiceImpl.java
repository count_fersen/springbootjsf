package id.co.rhs.spring.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import id.co.rhs.daos.IUserDao;
import id.co.rhs.models.User;

/**
 * A spring {@link UserDetailsService} implementation class.
 *
 * @author Rochmat Santoso
 * */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    /**
     * User DAO bean.
     * */
    @Autowired
    private IUserDao userDao;

    @Override
    public final UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {
        User user = userDao.getUserByUsername(username);

        String[] roles = user.getRoles().split(",");
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (String role : roles) {
            SimpleGrantedAuthority auth = new SimpleGrantedAuthority(role);
            authorities.add(auth);
        }
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                authorities);
    }

}
