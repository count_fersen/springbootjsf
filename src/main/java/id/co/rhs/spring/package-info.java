/**
 * A parent package that contains all spring configuration classes.
 *
 * @author Rochmat Santoso
 * */
package id.co.rhs.spring;
