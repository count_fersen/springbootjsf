package id.co.rhs.spring;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * The spring boot starter class.
 *
 * @author Rochmat Santoso
 * */
@SpringBootApplication
@ComponentScan(basePackages = {"id.co.rhs.jsf", "id.co.rhs.spring"})
@MapperScan(basePackages = {"id.co.rhs.daos"})
public class App {

    /**
     * Public constructor.
     * */
    public App() {
    }

    /**
     * Main method.
     *
     * @param args main arguments
     * */
    public static void main(final String[] args) {
        SpringApplication.run(App.class, args);
    }

}
