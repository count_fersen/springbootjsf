package id.co.rhs.spring;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import id.co.rhs.utils.CommonUtil;

/**
 * A class that holds configuration for messaging service.
 *
 * @author Rochmat Santoso
 * */
@Configuration
@EnableJms
public class MessagingConfigurer {

    /**
     * ActiveMQ URL.
     * */
    @Value("${spring.activemq.broker-url}")
    private String mqUrl;

    /**
     * ActiveMQ username.
     * */
    @Value("${spring.activemq.user}")
    private String mqUsername;

    /**
     * ActiveMQ password.
     * */
    @Value("${spring.activemq.password}")
    private String mqPassword;

    /**
     * A mandatory flag indicating all classes are trusted to be consumed by
     * ActiveMQ.
     * */
    @Value("${spring.activemq.packages.trust-all}")
    private String mqTrustFlag;

    /**
     * A flag that indicates if we want to use topic or queue.
     * */
    @Value("${spring.activemq.isQueue}")
    private String isQueueStr;

    /**
     * Get ActiveMQ password.
     *
     * @return the database password
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    private String getSecurePassword()
            throws InvalidKeyException,
                   UnsupportedEncodingException,
                   NoSuchAlgorithmException,
                   NoSuchPaddingException,
                   IllegalBlockSizeException,
                   BadPaddingException {
        return CommonUtil.decrypt(mqPassword);
    }

    /**
     * Create ActiveMQ connection factory bean.
     *
     * @return ActiveMQ connection factory
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    @Bean
    public ActiveMQConnectionFactory connectionFactory()
            throws InvalidKeyException,
                   UnsupportedEncodingException,
                   NoSuchAlgorithmException,
                   NoSuchPaddingException,
                   IllegalBlockSizeException,
                   BadPaddingException {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(mqUrl);
        connectionFactory.setUserName(mqUsername);
        connectionFactory.setPassword(getSecurePassword());
        connectionFactory.setTrustAllPackages(Boolean.parseBoolean(mqTrustFlag));
        return connectionFactory;
    }

    /**
     * Create a JMS template bean.
     *
     * @return JMS template object
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    @Bean
    public JmsTemplate jmsTemplate() throws InvalidKeyException,
            UnsupportedEncodingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            IllegalBlockSizeException,
            BadPaddingException {
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());

        boolean isQueue = Boolean.parseBoolean(isQueueStr);
        if (!isQueue) {
            // it's a topic
            template.setPubSubDomain(true);
        }

        return template;
    }

    /**
     * Create JMS listener container factory bean.
     *
     * @return JMS listener container factory bean
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory()
            throws InvalidKeyException,
                   UnsupportedEncodingException,
                   NoSuchAlgorithmException,
                   NoSuchPaddingException,
                   IllegalBlockSizeException,
                   BadPaddingException {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());

        boolean isQueue = Boolean.parseBoolean(isQueueStr);
        if (!isQueue) {
            // it's a topic
            factory.setConcurrency("1-1"); // we should have only 1 subscriber or else we will receive many messages
            factory.setPubSubDomain(true);
            factory.setClientId("durableSubscriberRHS");
            factory.setSubscriptionDurable(true);
        } else {
            factory.setConcurrency("1-10"); // we may have more than 1 consumer since message will only be sent once
        }

        return factory;
    }

}
