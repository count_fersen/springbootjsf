package id.co.rhs.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import id.co.rhs.spring.security.PasswordEncoderImpl;

/**
 * An implementation of Spring security WebSecurityConfigurerAdapter.
 *
 * @author Rochmat Santoso
 * */
@Configuration
@EnableWebSecurity
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    /**
     * User details service to get user info.
     * */
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Get password encoder.
     *
     * @return the encoder object
     * */
    @Bean
    public PasswordEncoderImpl passwordEncoder() {
        return new PasswordEncoderImpl();
    }

    /**
     * Configure authentication.
     *
     * @param auth AuthenticationManagerBuilder
     * @throws Exception Exception
     * */
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected final void configure(final HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/javax.faces.resource/**").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login.xhtml").permitAll()
                .failureUrl("/login.xhtml?error=true")
                .and()
            .logout()
                .logoutSuccessUrl("/login.xhtml").permitAll();
        http.csrf().disable();
    }

}
