package id.co.rhs.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * An implementation of Spring WebMvcConfigurer.
 *
 * @author Rochmat Santoso
 * */
@Configuration
public class WelcomePageConfigurer implements WebMvcConfigurer {

    @Override
    public final void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/greeting.xhtml");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);

        WebMvcConfigurer.super.addViewControllers(registry);
    }

}
