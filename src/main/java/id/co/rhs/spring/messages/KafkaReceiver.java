package id.co.rhs.spring.messages;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import id.co.rhs.models.Customer;

/**
 * A class that receives messages from kafka topic.
 *
 * @author Rochmat Santoso
 * */
@Component
public class KafkaReceiver {

    /**
     * Receive a Customer message from topic.
     *
     * @param customer the Customer message to receive
     * */
    @KafkaListener(topics = "mq.request")
    public void receive(final Customer customer) {
        System.out.println(customer);
    }

}
