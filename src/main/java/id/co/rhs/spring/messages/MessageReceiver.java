package id.co.rhs.spring.messages;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import id.co.rhs.models.Customer;

/**
 * A class that receives messages from ActiveMQ.
 *
 * @author Rochmat Santoso
 * */
@Component
public class MessageReceiver {

    /**
     * Receive message.
     *
     * @param customer customer object to receive
     * */
    @JmsListener(destination = "mq.request")
    public void receive(final Customer customer) {
        System.out.println("Registration received " + customer);
    }

}
