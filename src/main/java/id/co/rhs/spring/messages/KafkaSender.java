package id.co.rhs.spring.messages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import id.co.rhs.models.Customer;

/**
 * A class that send messages to kafka topic.
 *
 * @author Rochmat Santoso
 * */
@Component
public class KafkaSender {

    /**
     * A kafka template bean.
     * */
    @Autowired
    private KafkaTemplate<String, Customer> kafkaTemplate;

    /**
     * Send a Customer object as message to kafka topic.
     *
     * @param customer the Customer to send
     * */
    public void send(final Customer customer) {
        kafkaTemplate.send("mq.request", customer);
    }

}
