package id.co.rhs.spring.messages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import id.co.rhs.models.Customer;

/**
 * A class that sends messages to ActiveMQ.
 *
 * @author Rochmat Santoso
 * */
@Component
public class MessageSender {

    /**
     * JMS template object for sending message.
     * */
    @Autowired
    private JmsTemplate jmsTemplate;

    /**
     * Send message to ActiveMQ.
     *
     * @param customer customer object to send
     * */
    public void send(final Customer customer) {
        jmsTemplate.convertAndSend("mq.request", customer);
    }

}
