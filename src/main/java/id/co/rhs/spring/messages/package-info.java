/**
 * A package that contains all classes related to messaging.
 *
 * @author Rochmat Santoso
 *
 */
package id.co.rhs.spring.messages;
