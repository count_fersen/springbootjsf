/**
 * A package that contains all model classes.
 *
 * @author Rochmat Santoso
 * */
package id.co.rhs.models;
