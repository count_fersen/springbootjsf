package id.co.rhs.models;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A model class that represents customer.
 *
 * @author Rochmat Santoso
 * */
public class Customer extends BaseModel {

    /**
     *
     */
    private static final long serialVersionUID = 2367062197294886152L;

    /**
     * Customer's first name.
     * */
    private String firstName;

    /**
     * Customer's last name.
     * */
    private String lastName;

    /**
     * Customer's date of birth.
     * */
    private Date dateOfBirth;

    /**
     * Customer's address.
     * */
    private Address address;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param paramFirstName the firstName to set
     */
    public void setFirstName(final String paramFirstName) {
        this.firstName = paramFirstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param paramLastName the lastName to set
     */
    public void setLastName(final String paramLastName) {
        this.lastName = paramLastName;
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param paramDateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(final Date paramDateOfBirth) {
        this.dateOfBirth = paramDateOfBirth;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param paramAddress the address to set
     */
    public void setAddress(final Address paramAddress) {
        this.address = paramAddress;
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("firstName", firstName)
                .append("lastName", lastName)
                .append("dateOfBirth", dateOfBirth)
                .append("address", address).build();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        Customer target = (Customer) obj;
        return new EqualsBuilder().append(this.getId(), target.getId())
                .append(this.firstName, target.firstName)
                .append(this.lastName, target.lastName)
                .append(this.address, target.address)
                .append(this.dateOfBirth, target.dateOfBirth).isEquals();
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder().append(this.getId())
                .append(this.firstName)
                .append(this.lastName)
                .append(this.address)
                .append(this.dateOfBirth).toHashCode();
    }

}
