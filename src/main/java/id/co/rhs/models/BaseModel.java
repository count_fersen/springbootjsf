package id.co.rhs.models;

import java.io.Serializable;

/**
 * A class that acts as a parent of all model classes.
 *
 * @author Rochmat Santoso
 * */
public class BaseModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8989753060678703187L;

    /**
     * Primary key.
     * */
    private String id;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param paramId the id to set
     */
    public void setId(final String paramId) {
        this.id = paramId;
    }

}
