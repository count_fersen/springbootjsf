package id.co.rhs.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A model class that represents customer's address.
 *
 * @author Rochmat Santoso
 * */
public class Address extends BaseModel {

    /**
     *
     */
    private static final long serialVersionUID = 231353268759539641L;

    /**
     * Customer's address.
     * */
    private String address;

    /**
     * Customer's city address.
     * */
    private String city;

    /**
     * Customer's address postal code.
     * */
    private String postalCode;

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param paramAddress the address to set
     */
    public void setAddress(final String paramAddress) {
        this.address = paramAddress;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param paramCity the city to set
     */
    public void setCity(final String paramCity) {
        this.city = paramCity;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param paramPostalCode the postalCode to set
     */
    public void setPostalCode(final String paramPostalCode) {
        this.postalCode = paramPostalCode;
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("address", address)
                .append("city", city)
                .append("postalCode", postalCode).build();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        Address target = (Address) obj;
        return new EqualsBuilder().append(this.getId(), target.getId())
                .append(this.address, target.address)
                .append(this.city, target.city)
                .append(this.postalCode, target.postalCode).isEquals();
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder().append(this.getId())
                .append(this.address)
                .append(this.city)
                .append(this.postalCode).toHashCode();
    }

}
