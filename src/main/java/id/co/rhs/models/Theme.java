package id.co.rhs.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A model class that represents Primefaces theme.
 *
 * @author Rochmat Santoso
 */
public class Theme extends BaseModel {

    /**
     *
     */
    private static final long serialVersionUID = 7071132412884027107L;

    /**
     * Constructor using fields.
     *
     * @param paramDisplayName the theme's display name
     * @param paramName the theme's actual name
     * */
    public Theme(final String paramDisplayName, final String paramName) {
        super();
        this.displayName = paramDisplayName;
        this.name = paramName;
    }

    /**
     * Theme's displayed name.
     * */
    private String displayName;

    /**
     * Theme's actual name.
     * */
    private String name;

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param paramDisplayName the displayName to set
     */
    public void setDisplayName(final String paramDisplayName) {
        this.displayName = paramDisplayName;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param paramName the name to set
     */
    public void setName(final String paramName) {
        this.name = paramName;
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder().append(this.getId())
                .append(this.displayName)
                .append(this.name).toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        Theme target = (Theme) obj;
        return new EqualsBuilder().append(this.getId(), target.getId())
                .append(this.displayName, target.displayName)
                .append(this.name, target.name).isEquals();
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("displayName", displayName)
                .append("name", name).build();
    }

}
