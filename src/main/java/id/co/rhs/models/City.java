package id.co.rhs.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A model class that represents a city.
 *
 * @author Rochmat Santoso
 * */
public class City extends BaseModel {

    /**
     *
     */
    private static final long serialVersionUID = 1117384084396902650L;

    /**
     * City name.
     * */
    private String name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param paramName the name to set
     */
    public void setName(final String paramName) {
        this.name = paramName;
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("name", name).build();
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder().append(this.getId())
                .append(this.name).toHashCode();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        City target = (City) obj;
        return new EqualsBuilder().append(this.getId(), target.getId())
                .append(this.name, target.name).isEquals();
    }

}
