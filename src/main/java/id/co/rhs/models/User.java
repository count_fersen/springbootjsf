package id.co.rhs.models;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A model class that represents user.
 *
 * @author Rochmat Santoso
 * */
public class User extends BaseModel {

    /**
     *
     */
    private static final long serialVersionUID = -7405366567243414417L;

    /**
     * Username.
     * */
    private String username;

    /**
     * Password.
     * */
    private String password;

    /**
     * User's roles comma separated.
     * */
    private String roles;

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param paramUsername the username to set
     */
    public void setUsername(final String paramUsername) {
        this.username = paramUsername;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param paramPassword the password to set
     */
    public void setPassword(final String paramPassword) {
        this.password = paramPassword;
    }

    /**
     * @return the roles
     */
    public String getRoles() {
        return roles;
    }

    /**
     * @param paramRoles the roles to set
     */
    public void setRoles(final String paramRoles) {
        this.roles = paramRoles;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        User target = (User) obj;
        return new EqualsBuilder().append(this.getId(), target.getId())
                .append(this.username, target.username)
                .append(this.password, target.password)
                .append(this.roles, target.roles).isEquals();
    }

    @Override
    public final int hashCode() {
        return new HashCodeBuilder().append(this.getId())
                .append(this.username)
                .append(this.password)
                .append(this.roles).toHashCode();
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this).append("id", getId())
                .append("username", username)
                .append("password", password)
                .append("roles", roles).build();
    }

}
