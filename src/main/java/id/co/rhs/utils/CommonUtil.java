package id.co.rhs.utils;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A class that contains all common utility methods and global variables.
 *
 * @author Rochmat Santoso
 * */
public final class CommonUtil {

    /**
     * Class logger.
     * */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Global date format.
     * */
    public static final SimpleDateFormat DATE_FORMAT;

    /**
     * Global date time format.
     * */
    public static final SimpleDateFormat DATETIME_FORMAT;

    /**
     * Default theme from properties.
     * */
    public static final String DEFAULT_THEME;

    static {
        LOGGER.info("Loading properties file...");

        Properties properties = new Properties();
        try {
            InputStream inStream = CommonUtil.class.getClassLoader().getResourceAsStream("application.properties");
            properties.load(inStream);
            LOGGER.info("Success loading properties file...");
        } catch (Exception e) {
            LOGGER.error("Error loading properties file...");
            e.printStackTrace();
        }

        DATE_FORMAT = new SimpleDateFormat(properties.getProperty("format.date"));
        DATETIME_FORMAT = new SimpleDateFormat(properties.getProperty("format.datetime"));
        DEFAULT_THEME = properties.getProperty("jsf.primefaces.theme");
    }

    /**
     * Private constructor.
     * */
    private CommonUtil() {
    }

    /**
     * Format date time.
     *
     * @param date the date time to be formatted
     * @return formatted date time
     * */
    public static String formatDateTime(final Date date) {
        return DATETIME_FORMAT.format(date);
    }

    /**
     * Format date.
     *
     * @param date the date to be formatted
     * @return formatted date
     * */
    public static String formatDate(final Date date) {
        return DATE_FORMAT.format(date);
    }

    /**
     * Encrypt text using MD5.
     *
     * @param plainText the text to encrypt
     * @return encrypted text
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    public static String encryptMD5(final String plainText)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String result = null;
        byte[] plainByte = plainText.getBytes("UTF8");

        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(plainByte);

        byte[] cipherByte = digest.digest();
        byte[] resultByte = Base64.getEncoder().encode(cipherByte);
        result = new String(resultByte);

        return result;
    }

    /**
     * Encrypt a plain text.
     *
     * @param plainText the plain text to encrypt
     * @return encrypted plain text
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    public static String encrypt(final String plainText)
            throws UnsupportedEncodingException,
                   NoSuchAlgorithmException,
                   NoSuchPaddingException,
                   InvalidKeyException,
                   IllegalBlockSizeException,
                   BadPaddingException {
        String result = null;

        // get UTF8 encoded byte
        byte[] plainByte = plainText.getBytes("UTF8");

        // encrypting
        String keyText = "kahakahakahakaha";
        byte[] keyByte = keyText.getBytes("UTF8");

        SecretKey secretKey = new SecretKeySpec(keyByte, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] cipherByte = cipher.doFinal(plainByte);

        // encode base64
        byte[] resultByte = Base64.getEncoder().encode(cipherByte);
        result = new String(resultByte);

        return result;
    }

    /**
     * Decrypt a text.
     *
     * @param cipherText the text to decrypt
     * @return decrypted text
     * @throws InvalidKeyException InvalidKeyException
     * @throws BadPaddingException BadPaddingException
     * @throws IllegalBlockSizeException IllegalBlockSizeException
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     * @throws NoSuchPaddingException NoSuchPaddingException
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     * */
    public static String decrypt(final String cipherText)
            throws UnsupportedEncodingException,
                   NoSuchAlgorithmException,
                   NoSuchPaddingException,
                   InvalidKeyException,
                   IllegalBlockSizeException,
                   BadPaddingException {
        String result = null;

        // get decoded base64 byte
        byte[] cipherByte = Base64.getDecoder().decode(cipherText);

        // decrypting
        String keyText = "kahakahakahakaha";
        byte[] keyByte = keyText.getBytes("UTF8");

        SecretKey secretKey = new SecretKeySpec(keyByte, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] plainByte = cipher.doFinal(cipherByte);

        // get UTF8 encoded string
        result = new String(plainByte, "UTF8");

        return result;
    }

}
