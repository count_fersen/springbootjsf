/**
 * A package that contains all test classes.
 *
 * @author Rochmat Santoso
 */
package id.co.rhs.tests;
