package id.co.rhs.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * A test suite class.
 *
 * @author Rochmat Santoso
 * */
@RunWith(Suite.class)
@SuiteClasses({ CustomerTestCase.class })
public class AllTests {

}
