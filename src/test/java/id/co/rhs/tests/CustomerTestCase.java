package id.co.rhs.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import id.co.rhs.daos.IAddressDao;
import id.co.rhs.daos.ICustomerDao;
import id.co.rhs.jsf.CustomerController;
import id.co.rhs.models.Address;
import id.co.rhs.models.Customer;

/**
 * A class that contains all test cases related to customer.
 *
 * @author Rochmat Santoso
 * */
@RunWith(MockitoJUnitRunner.class)
public class CustomerTestCase {

    /**
     * Mocked customer DAO.
     * */
    @Mock
    private ICustomerDao customerDao;

    /**
     * Mocked address DAO.
     * */
    @Mock
    private IAddressDao addressDao;

    /**
     * Mocked customer.
     * */
    @Mock
    private Customer customer;

    /**
     * Controller class being tested. All mocked objects are injected here.
     * */
    @InjectMocks
    private CustomerController customerController;

    /**
     * A method that is called before test case is run.
     * */
    @Before
    public void before() {
        System.out.println("Running Customer test cases...");
    }

    /**
     * A method that is run after test case is finished.
     * */
    @After
    public void after() {
        System.out.println("Finished running Customer test cases");
    }

    /**
     * Test case that insert a customer with address information.
     * */
    @Test
    public void testInsertCustomerWithAddress() {
        Address address = new Address();
        address.setAddress("Bronggalan Sawah");
        address.setCity("Surabaya");
        address.setPostalCode("60132");

        Mockito.when(customer.getAddress()).thenReturn(address);
        customerController.addCustomer();
    }

    /**
     * Test case that insert customer data without address information.
     * */
    @Test
    public void testInsertCustomerWithoutAddress() {
        customerController.addCustomer();
    }

}
